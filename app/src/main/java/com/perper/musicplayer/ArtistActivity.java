package com.perper.musicplayer;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.perper.musicplayer.model.Album;
import com.perper.musicplayer.model.Artist;
import com.perper.musicplayer.model.QueryContent;
import com.perper.musicplayer.service.PlayerService;
import com.perper.musicplayer.service.PlayerServiceConnector;

import java.util.List;

public class ArtistActivity extends AppCompatActivity {
    private static final String TAG = "MusicPlayer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "ArtistActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);

        StatusBar statusBar = new StatusBar(this);

        Artist artist = Artist.get(getIntent());

        this.getSupportActionBar().setSubtitle("Albums de " + artist.name);


        populateListView(artist);
    }

    private void populateListView(Artist artist) {
        List<Album> albums = QueryContent.getAlbums(getContentResolver(), artist);
        // Get ListView object from xml
        final ListView listView = (ListView) findViewById(R.id.albumsListView);

        ArrayAdapter<Album> adapter = new ArrayAdapter<Album>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, albums);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                Album  album    = (Album) listView.getItemAtPosition(position);

                Log.d("MusicPlayer", "Must open album " + album.name);

                Intent intent = new Intent(ArtistActivity.this, AlbumActivity.class);
                album.put(intent);
                startActivity(intent);
            }

        });
    }


}
