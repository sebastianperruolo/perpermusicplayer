package com.perper.musicplayer.service;

import com.perper.musicplayer.model.Song;

import java.util.List;

/**
 * Created by jose on 17/10/16.
 */

public interface IPlayerServiceBinder {
    void reproducir(List<Song> album);
    void reproducir(Song cancion);
    void parar();
    void nextSong();
    void pause();

    void reproducir(List<Song> songs, int position);
}
