package com.perper.musicplayer.service;

import android.os.Binder;

import com.perper.musicplayer.model.Song;

import java.util.List;

/**
 * Created by jose on 17/10/16.
 */

public class PlayerMusicBinder extends Binder implements IPlayerServiceBinder {
    PlayerService playerService;

    public PlayerMusicBinder(PlayerService playerService) {
        this.playerService = playerService;
    }

    @Override
    public void reproducir(List<Song> album) {
        playerService.playAlbum(album);
    }

    @Override
    public void reproducir(Song cancion) {
        playerService.play(cancion);
    }

    @Override
    public void parar() {
        playerService.stop();
    }

    @Override
    public void nextSong() {
        playerService.nextSong();
    }

    @Override
    public void pause() {
        playerService.pause();
    }

    @Override
    public void reproducir(List<Song> songs, int position) {
        playerService.playAlbum(songs,position);
    }

}