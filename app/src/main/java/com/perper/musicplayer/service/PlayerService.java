package com.perper.musicplayer.service;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.perper.musicplayer.model.Song;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jose on 17/10/16.
 */

public class PlayerService extends Service {

    public static final String PLAYING_SONG = "playing";
    public static final String NAME_SONG = "nombre";

    public LocalBroadcastManager broadcastManager;

    private MediaPlayer mMediaPlayer;
    private ServiceThread serviceThread;
    private int cancionActual;
    private int ultimaCancion;
    private List<Song> album;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new PlayerMusicBinder(this);
    }
    public void playAlbum(List<Song> album){
        playAlbum(album,0);
    }
    public void play(Song song) {
        Uri contentUri = ContentUris.withAppendedId(
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, song.id);
        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
        } else {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
        }

        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mMediaPlayer.setDataSource(getApplicationContext(), contentUri);

            mMediaPlayer.prepare();
            mMediaPlayer.start();

            Intent i = new Intent(PLAYING_SONG);
            i.putExtra(NAME_SONG, song.title);
            broadcastManager.sendBroadcast(i);

            Log.d("MusicPlayer", "Playing song " + song.title);
        } catch (IOException e) {
            Log.e("MusicPlayer", "Error playing song "+ e.getMessage(), e);
        }
        //mMediaPlayer.release();
        //mMediaPlayer = null;

    }
    public void stop(){
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
        }
        Intent i = new Intent(PLAYING_SONG);
        i.putExtra(NAME_SONG, "");
        broadcastManager.sendBroadcast(i);
    }

    public void nextSong() {
        cancionActual++;
        if(cancionActual < ultimaCancion){
            play(album.get(cancionActual));
        }
    }

    public void pause() {
        if (!mMediaPlayer.isPlaying()) {
            mMediaPlayer.start();
        } else {
            mMediaPlayer.pause();
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        this.broadcastManager = LocalBroadcastManager.getInstance(this);
        this.serviceThread=new ServiceThread();
        this.serviceThread.start();


        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
        }
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){

            @Override
            public void onCompletion(MediaPlayer mp) {
                nextSong();
            }
        });

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        this.serviceThread.requestStop();
    }

    public void playAlbum(List<Song> songs, int position) {
        cancionActual = position;
        this.album= songs;
        ultimaCancion = album.size();

        play(this.album.get(position));
    }
}
