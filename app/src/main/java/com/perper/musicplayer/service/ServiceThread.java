package com.perper.musicplayer.service;

import android.os.Looper;

/**
 * Created by jose on 24/10/16.
 */

public class ServiceThread extends Thread {

    private Looper looper;
    private PlayerServiceHandler handler;
    @Override
    public void run() {
        Looper.prepare();
        this.handler=new PlayerServiceHandler();
        this.looper=Looper.myLooper();
        Looper.loop();
    }

    public PlayerServiceHandler getHandler(){
        return this.handler;
    }

    public void requestStop() {
        this.looper.quit();
    }

}
