package com.perper.musicplayer.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.perper.musicplayer.StatusBar;

/**
 * Created by jose on 24/10/16.
 */

public class PlayerServiceConnector implements ServiceConnection {
    private final StatusBar statusBar;
    public PlayerServiceConnector(StatusBar statusBar) {
        super();
        this.statusBar = statusBar;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        this.statusBar.setPlayerMusicBinder((IPlayerServiceBinder) service);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        this.statusBar.setPlayerMusicBinder(null);
    }
}
