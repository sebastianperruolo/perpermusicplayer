package com.perper.musicplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.perper.musicplayer.model.Song;
import com.perper.musicplayer.service.IPlayerServiceBinder;
import com.perper.musicplayer.service.PlayerService;
import com.perper.musicplayer.service.PlayerServiceConnector;

import java.util.List;

/**
 * Created by sebastianperruolo on 10/11/16.
 */

public class StatusBar {
    private static final String TAG = "MusicPlayer";
    private final AppCompatActivity activity;
    private LocalBroadcastManager localBroadcastManager;
    private IPlayerServiceBinder playerMusicBinder;

    private TextView status;

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //que hacer!!
            Log.d(TAG,"onReceive");

            status.setText("Reproduciendo " + intent.getStringExtra(PlayerService.NAME_SONG));
        }
    };

    public StatusBar(AppCompatActivity activity) {
        this.activity = activity;

        status = (TextView) activity.findViewById(R.id.statusTextView);
        status.setText("Titulo");

        localBroadcastManager = LocalBroadcastManager.getInstance(activity);
        localBroadcastManager.registerReceiver(br, new IntentFilter(PlayerService.PLAYING_SONG));

        Intent i=new Intent(activity, PlayerService.class);
        ServiceConnection conn = new PlayerServiceConnector(this);
        activity.bindService(i,conn, Context.BIND_AUTO_CREATE);

        Button stop = (Button) activity.findViewById(R.id.stop);
        stop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                playerMusicBinder.parar();
            }
        });

        Button next = (Button) activity.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                playerMusicBinder.nextSong();
            }
        });

        Button pause = (Button) activity.findViewById(R.id.pause);
        pause.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                playerMusicBinder.pause();
            }
        });
    }


    public void setPlayerMusicBinder(IPlayerServiceBinder playerMusicBinder) {
        this.playerMusicBinder = playerMusicBinder;
    }

    public void onRestart() {
        localBroadcastManager.registerReceiver(br, new IntentFilter(PlayerService.PLAYING_SONG));
    }

    public void onPause() {
        localBroadcastManager.unregisterReceiver(br);
    }

    public void reproducir(List<Song> songs, int position) {
        playerMusicBinder.reproducir(songs, position);
    }
}
