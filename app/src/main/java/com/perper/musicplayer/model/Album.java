package com.perper.musicplayer.model;

import android.content.Intent;

/**
 * Created by sebastianperruolo on 4/11/16.
 */

public class Album {
    public final long id;
    public final String name;
    public final Artist artist;
    public final int numberOfTracks;
    public final String year;

    public Album(long id, String name, Artist artist, int numberOfTracks, String year) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.numberOfTracks = numberOfTracks;
        this.year = year;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static Album get(Intent intent) {
        long id = intent.getLongExtra("album/id", -1);
        String name = intent.getStringExtra("album/name");
        Artist artist = Artist.get(intent);
        int numberOfTracks = intent.getIntExtra("album/numberOfTracks", 0);
        String year = intent.getStringExtra("album/year");

        return new Album(id, name, artist, numberOfTracks, year);
    }

    public void put(Intent intent) {
        intent.putExtra("album/id", this.id);
        intent.putExtra("album/name", this.name);
        this.artist.put(intent);
        intent.putExtra("album/numberOfTracks", this.numberOfTracks);
        intent.putExtra("album/year", this.year);
    }
}
