package com.perper.musicplayer.model;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sebastianperruolo on 3/11/16.
 */

public class QueryContent {

    public static List<Artist> getArtists(final ContentResolver contentResolver) {
        Uri uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;

        String[] mProjection = {
            MediaStore.Audio.Artists._ID,
            MediaStore.Audio.Artists.ARTIST,
            MediaStore.Audio.Artists.NUMBER_OF_TRACKS,
            MediaStore.Audio.Artists.NUMBER_OF_ALBUMS
        };

        Cursor cursor = contentResolver.query(uri, mProjection, null, null, MediaStore.Audio.Artists.ARTIST + " ASC");

        if (cursor == null) {
            Log.d("MusicPlayer", "Query failed");
            return Collections.EMPTY_LIST;
        }
        if (!cursor.moveToFirst()) {
            Log.d("MusicPlayer", "No media on device");
            return Collections.EMPTY_LIST;
        }

        Log.d("MusicPlayer", "All good, artists found "+ cursor.getCount());
        // Los indices de las columnas corresponden a lo usado en mProjection, pero igual hago esto:
        int idColumn = cursor.getColumnIndex(MediaStore.Audio.Artists._ID);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST);
        int tracksNoColumn = cursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_TRACKS);
        int albumsNoColumn = cursor.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS);

        List<Artist> artists = new ArrayList<>(cursor.getCount());

        do {
            long id = cursor.getLong(idColumn);
            String artistName = cursor.getString(artistColumn);
            Log.d("MusicPlayer", "tracks type " + cursor.getType(tracksNoColumn));
            Log.d("MusicPlayer", "albums type " + cursor.getType(albumsNoColumn));
            int tracksNo = cursor.getInt(tracksNoColumn);
            int albumsNo = cursor.getInt(albumsNoColumn);
            final Artist artist = new Artist(id, artistName, tracksNo, albumsNo);
            Log.d("MusicPlayer", "artist found " + artist.toString());
            artists.add(artist);
        } while (cursor.moveToNext());

        cursor.close();
        return artists;
    }

    public static List<Album> getAlbums(final ContentResolver contentResolver, final Artist artist) {
        Uri uri = MediaStore.Audio.Artists.Albums.getContentUri("external", artist.id);

        String[] mProjection = {
            MediaStore.Audio.Albums._ID,
            MediaStore.Audio.Albums.ALBUM,
            MediaStore.Audio.Albums.ARTIST,
            MediaStore.Audio.Albums.NUMBER_OF_SONGS,
            MediaStore.Audio.Albums.FIRST_YEAR
        };

        Cursor cursor = contentResolver.query(uri, mProjection, null, null, MediaStore.Audio.Albums.DEFAULT_SORT_ORDER);
        if (cursor == null) {
            Log.d("MusicPlayer", "Query failed");
            return Collections.EMPTY_LIST;
        }
        if (!cursor.moveToFirst()) {
            Log.d("MusicPlayer", "No media on device");
            return Collections.EMPTY_LIST;
        }
        Log.d("MusicPlayer", "All good, albums found "+ cursor.getCount());

        // Los indices de las columnas corresponden a lo usado en mProjection, pero igual hago esto:
        int idColumn = cursor.getColumnIndex(MediaStore.Audio.Albums._ID);
        int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
        int songsNoColumn = cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);
        int firstYearColumn = cursor.getColumnIndex(MediaStore.Audio.Albums.FIRST_YEAR);

        ArrayList<Album> algums = new ArrayList<Album>();

        do {
            final long id = cursor.getLong(idColumn);
            final String albumName = cursor.getString(albumColumn);
            final int songCount = cursor.getInt(songsNoColumn);
            final String year = cursor.getString(firstYearColumn);
            final Album album = new Album(id, albumName, artist, songCount, year);
            Log.d("MusicPlayer", "Album found "+ album.toString());
            algums.add(album);
        } while (cursor.moveToNext());

        cursor.close();

        return algums;
    }

    public static List<Song> getAlbumSongs(final ContentResolver contentResolver, final Album album) {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] mProjection = {
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.TITLE
        };

        final StringBuilder selection = new StringBuilder();
        selection.append(MediaStore.Audio.AudioColumns.IS_MUSIC + "=1");
        selection.append(" AND " + MediaStore.Audio.AudioColumns.TITLE + " != ''");
        selection.append(" AND " + MediaStore.Audio.AudioColumns.ALBUM_ID + "=" + album.id);

        Cursor cursor = contentResolver.query(uri, mProjection, selection.toString(), null,
                MediaStore.Audio.Media.TRACK + ", " + MediaStore.Audio.Media.DEFAULT_SORT_ORDER);

        if (cursor == null) {
            Log.d("MusicPlayer", "Query failed");
            return Collections.EMPTY_LIST;
        }
        if (!cursor.moveToFirst()) {
            Log.d("MusicPlayer", "No media on device");
            return Collections.EMPTY_LIST;
        }
        Log.d("MusicPlayer", "All good, songs found "+ cursor.getCount());

        int idColumn = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
        int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);

        ArrayList<Song> songs = new ArrayList<Song>();
        do {
            final long id = cursor.getLong(idColumn);
            final String title = cursor.getString(titleColumn);
            final Song song = new Song(id, title, album);
            Log.d("MusicPlayer", "Song found "+ song.toString());
            songs.add(song);
        } while (cursor.moveToNext());

        cursor.close();

        return songs;
    }

}
