package com.perper.musicplayer.model;

import android.content.Intent;

/**
 * Created by sebastianperruolo on 3/11/16.
 */

public class Artist {
    public final long id;
    public final String name;
    public final int numberOfTracks;
    public final int numberOfAlbums;


    public Artist(long id, String name, int numberOfTracks, int numberOfAlbums) {
        this.id = id;
        this.name = name;
        this.numberOfTracks = numberOfTracks;
        this.numberOfAlbums = numberOfAlbums;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static Artist get(Intent intent) {
        long id = intent.getLongExtra("artist/id", -1);
        String name = intent.getStringExtra("artist/name");
        int numberOfTracks = intent.getIntExtra("artist/numberOfTracks", 0);
        int numberOfAlbums = intent.getIntExtra("artist/numberOfAlbums", 0);
        return new Artist(id, name, numberOfTracks, numberOfAlbums);
    }

    public void put(Intent intent) {
        intent.putExtra("artist/id", this.id);
        intent.putExtra("artist/name", this.name);
        intent.putExtra("artist/numberOfTracks", this.numberOfTracks);
        intent.putExtra("artist/numberOfAlbums", this.numberOfAlbums);
    }
}
