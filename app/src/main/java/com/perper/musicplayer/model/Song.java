package com.perper.musicplayer.model;

/**
 * Created by sebastianperruolo on 10/10/16.
 */

public class Song {
    public final long id;
    public final String title;
    public final Album album;

    public Song(long id, String title, Album album) {
        this.id = id;
        this.title = title;
        this.album = album;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
