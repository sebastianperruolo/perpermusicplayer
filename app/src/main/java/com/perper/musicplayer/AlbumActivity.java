package com.perper.musicplayer;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.perper.musicplayer.model.Album;
import com.perper.musicplayer.model.QueryContent;
import com.perper.musicplayer.model.Song;
import com.perper.musicplayer.service.PlayerService;

import java.util.List;

public class AlbumActivity extends AppCompatActivity {

    private static final String TAG = "MusicPlayer";

    private List<Song> songs;

    private StatusBar statusBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "AlbumActivity.onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        statusBar = new StatusBar(this);

        Album album = Album.get(getIntent());

        this.getSupportActionBar().setSubtitle("Canciones de " + album.name);


        Button playAlbum = (Button) findViewById(R.id.button_play_album);
        playAlbum.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                statusBar.reproducir(songs,0);
            }
        });


        populateListView(album);
    }

    private void populateListView(Album album) {
        songs = QueryContent.getAlbumSongs(getContentResolver(), album);
        // Get ListView object from xml
        final ListView listView = (ListView) findViewById(R.id.songsListView);

        ArrayAdapter<Song> adapter = new ArrayAdapter<Song>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, songs);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                statusBar.reproducir(songs,position);

            }

        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity","onRestart");

        statusBar.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        statusBar.onPause();
    }


}
